import { AdjaracmsPage } from './app.po';

describe('adjaracms App', () => {
  let page: AdjaracmsPage;

  beforeEach(() => {
    page = new AdjaracmsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
