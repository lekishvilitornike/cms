import {Directive, ElementRef, Output, EventEmitter, HostListener, Inject} from "@angular/core";

@Directive({
    selector: '[leave]'
})
export class LeaveDirective {

    constructor(@Inject(ElementRef) private elementRef: ElementRef) {
    }

    @Output() public leave: EventEmitter<any> = new EventEmitter();

    @HostListener('document:click', ['$event.target'])
    public click(element: any) {

        const leave = this.elementRef.nativeElement.contains(element);

        if (!leave) {
            this.leave.emit(null);
        }

    }
}