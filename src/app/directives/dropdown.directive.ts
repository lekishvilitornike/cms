import {Directive, ElementRef, HostListener, Inject, Input} from '@angular/core';

@Directive({
    selector: '[dropdown]'
})
export class DropdownDirective {

    @Input('dropdown') public className: string;

    private active: boolean = false;

    private element: HTMLElement;

    constructor(ref: ElementRef) {
        this.element = ref.nativeElement;
    }

    @HostListener('document:click', ['$event.target'])
    public click(element: any) {
        this.setActive(this.element.contains(element) && !this.active);
    }

    public setActive(active: boolean) {
        this.active = active;
        this.element.classList[this.active ? 'add' : 'remove'](this.className);
    }
}