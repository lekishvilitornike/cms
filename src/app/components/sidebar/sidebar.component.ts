import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {SettingsService} from '../../services/settings.service';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.less'],
    encapsulation: ViewEncapsulation.None,
})
export class SidebarComponent implements OnInit {

    constructor(public settings: SettingsService,
                public auth: AuthService) {
    }

    ngOnInit() {
    }
}
