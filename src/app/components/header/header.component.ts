import {Component, ViewEncapsulation} from '@angular/core';
import {LocaleService} from '../../services/locale.service';
import {SettingsService} from '../../services/settings.service';
import {AuthService} from '../../services/auth.service';
import {PlayerService} from '../../modules/player/player.service';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['./header.component.less'],
    encapsulation: ViewEncapsulation.None,
})
export class HeaderComponent {

    public collapsed: boolean = false;

    constructor(public localization: LocaleService,
                public settings: SettingsService,
                public auth: AuthService,
                public player: PlayerService) {
    }

    toggleCollapse() {
        this.collapsed = !this.collapsed;
    }
}