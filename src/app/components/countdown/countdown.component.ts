import {Component, OnInit} from '@angular/core';
import {Api} from '../../api';
import {Observable} from 'rxjs/Observable';

@Component({
    templateUrl: 'countdown.component.html',
})
export class CountdownComponent implements OnInit {

    public resource: (query: any) => Observable<any>;

    public models: any[];

    public model: any;

    public numbers: any[] = [
        {value: 9},
        {value: 5},
        {value: 3},
        {value: 1},
        {value: 3},
        {value: 7},
        {value: 9}
    ];

    public duration: number = 1000;

    constructor(private api: Api) {
    }

    ngOnInit(): any {
        this.resource = (query) => this.api.resource('tags/suggestions').get(query);

        let i: number;

        setInterval(() => {
            for (i = this.numbers.length - 1; i >= 0; i--) {
                this.animate(this.numbers[i]);
                if (this.numbers[i].value > 0) {
                    break;
                }
            }
        }, this.duration);
    }

    animate(numb) {
        numb.className = 'next';
        setTimeout(() => {
            numb.value = (numb.value + 9) % 10;
            numb.className = '';
        }, this.duration * 0.8);
    }
}
