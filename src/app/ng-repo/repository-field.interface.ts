export interface IRepositoryField {
    title: string;
    name: string;
    type: string;
    hidden: boolean;
    classList?: string[];
}
