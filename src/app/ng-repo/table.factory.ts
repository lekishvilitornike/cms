import {Injectable} from '@angular/core';
import {INgColumn, NgTable} from '../modules/ng-table/ng-table';
import {Api} from '../api';
import {IRepositoryField} from './repository-field.interface';
import {IRepository} from './repository.interface';
import {SettingsService} from '../services/settings.service';
import {DatePipe} from '@angular/common';

@Injectable()
export class NgTableFactory {

    constructor(private api: Api,
                private settings: SettingsService) {
    }

    public create(repository: IRepository) {

        const columns: INgColumn[] = [];

        const fieldMap: Map<string, IRepositoryField> = this.keyBy(repository.fields, 'name');

        repository.columns.forEach((column: string) => {
            if (fieldMap.has(column)) {
                const iNgColumn: INgColumn = this.createColumn(fieldMap.get(column));
                columns.push(iNgColumn);
            }
        });

        const resource = (query: any) => this.api.resource(repository.endpoint).get(query);

        return new NgTable(resource, columns);
    }

    private keyBy<T>(array: T[], keyName: string): Map<string, T> {
        const result: Map<string, T> = new Map();
        for (let i in array) {
            if (array.hasOwnProperty(i)) {
                result.set(array[i][keyName], array[i])
            }
        }
        return result;
    }

    private createColumn(field: IRepositoryField): INgColumn {

        const typePart: string[] = field.type.split(':');
        let classList: string[] = field.classList ? field.classList : [];
        let value: (item: any) => string;
        let type: string = 'text';

        switch (typePart[0]) {
            case 'model': {
                const relation: IRepository = this.settings.getRepositoryByName(typePart[1]);
                value = (item) => {
                    if (!item.hasOwnProperty(field.name)) {
                        return '';
                    }
                    return item[field.name][relation.label];
                };
                break;
            }
            case 'models': {
                const relation: IRepository = this.settings.getRepositoryByName(typePart[1]);
                value = (item) => {
                    if (!item.hasOwnProperty(field.name)) {
                        return '';
                    }
                    return item[field.name].map(item => item[relation.label]).join(', ');
                };
                break;
            }
            case 'file': {
                let config: any = this.settings.getFileConfigByName(typePart[1]);
                type = config ? config.type : '';
                value = (item) => item[field.name] ? item[field.name]['url'] : null;
                break;
            }
            case 'date': {
                value = (item) => (new DatePipe('en')).transform(item[field.name], 'yyyy.MMM.dd HH:mm');
                classList.push('text-center');
                break;
            }
            default: {
                value = (item) => item[field.name];
            }
        }

        return {
            name: field.name,
            title: field.name,
            type: type,
            classList: classList,
            value: value
        };
    }
}

