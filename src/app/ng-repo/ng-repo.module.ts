import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgFormFactory} from './form.factory';
import {NgTableFactory} from './table.factory';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
    ],
    providers: [
        NgFormFactory,
        NgTableFactory,
    ],
})
export class NgRepoModule {
}
