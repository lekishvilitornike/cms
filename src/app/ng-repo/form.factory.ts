import {ComponentRef, Injectable} from '@angular/core';
import {INgInput, NgInput} from '../modules/ng-form/ng-input';
import {IRepository} from './repository.interface';
import {SettingsService} from '../services/settings.service';
import {Api} from '../api';
import {IRepositoryField} from './repository-field.interface';
import {EntityNgForm} from '../modules/ng-form/entity-ng-form';
import {IODefaultComponent} from '../modules/ng-inputs/inputs/default/default.component';
import {IOCheckboxComponent} from '../modules/ng-inputs/inputs/checkbox/checkbox.component';
import {IODatePickerComponent} from '../modules/ng-inputs/inputs/datepicker/date-picker.component';
import {IOSelectComponent} from '../modules/ng-inputs/inputs/select/select.component';
import {IOAutoCompleteComponent} from '../modules/ng-inputs/inputs/auto-complete/auto-complete.component';
import {IOMediaComponent} from '../modules/ng-inputs/inputs/media/media.component';
import {IOTableComponent} from '../modules/ng-inputs/inputs/table/table.component';
import {NgTableFactory} from './table.factory';
import {NgTable} from '../modules/ng-table/ng-table';
import {IOFormComponent} from '../modules/ng-inputs/inputs/form/form.component';
import {DialogService} from '../modules/dialog/dialog.service';
import {IOQuillComponent} from '../modules/ng-inputs/inputs/quill/quill.component';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class NgFormFactory {

    private components: { [name: string]: any } = {
        number: IODefaultComponent,
        text: IODefaultComponent,
        boolean: IOCheckboxComponent,
        date: IODatePickerComponent,
        select: IOSelectComponent,
        html: IOQuillComponent,
        model: IOAutoCompleteComponent,
        models: IOAutoCompleteComponent,
        file: IOMediaComponent,
        table: IOTableComponent,
    };

    constructor(private api: Api,
                private dialog: DialogService,
                private settings: SettingsService,
                private tableFactory: NgTableFactory) {
    }

    private createInput(field: IRepositoryField, repository: IRepository): INgInput {
        const typePart: string[] = field.type.split(':');
        if (!this.components.hasOwnProperty(typePart[0])) {
            throw new Error('Component for "' + typePart[0] + '" is not defined!');
        }

        const iNgInput: INgInput = {
            name: field.name,
            hidden: field.hidden,
            component: this.components[typePart[0]],
            params: []
        };

        switch (typePart[0]) {
            case 'model':
            case 'models': {
                const relationRepository: IRepository = this.settings.getRepositoryByName(typePart[1]);
                iNgInput.params['resource'] = (query) => this.api.resource(relationRepository.endpoint + '/suggestions').get(query);
                iNgInput.params['label'] = relationRepository.label;
                iNgInput.params['multiple'] = typePart[0] == 'models';
                break;
            }
            case 'html': {
                iNgInput.params['server'] = () => this.api.resource('files').post({type_id: 3});
                break;
            }
            case 'file': {
                iNgInput.params['config'] = this.settings.getFileConfigByName(typePart[1]);
                iNgInput.params['server'] = (query: any) => this.api.resource('files').post(query);
                break;
            }
            case 'table': {
                iNgInput.created = (input) => {
                    const callback = () => {
                        if (input.form.id) {
                            this.createRelatedRepository(typePart[1], input.form.id)
                                .subscribe((repository: IRepository) => {
                                    this.createRelatedTable(input, repository);
                                });
                        }
                    };
                    input.form.onLoad.subscribe(callback);
                    callback();
                };
                break;
            }
            case 'boolean': {
                iNgInput.value = false;
                break;
            }
            case 'number': {
                iNgInput.params['type'] = 'number';
                break;
            }
            case 'text': {
                iNgInput.params['type'] = 'text';
                break;
            }
        }

        return iNgInput;
    }

    public createRelatedRepository(name: string, parentId): Observable<IRepository> {
        const endpoint = this.settings.getRepositoryByName(name).endpoint.replace('{parentId}', parentId);
        return this.api.resource(endpoint + '/metadata').get()
            .map((repository: IRepository) => {
                repository.endpoint = endpoint;
                return repository;
            });
    }

    public createRelatedTable(input: NgInput, repository: IRepository) {
        const tableComponent: IOTableComponent = input.instance;
        const table: NgTable = this.tableFactory.create(repository);
        const createRelatedForm = (id?: any) => {
            this.createRelatedForm(repository, table, id);
        };
        table.columns.push({
            title: 'Edit',
            type: 'button',
            classList: ['col-xs-1'],
            action: (item) => createRelatedForm(item.id)
        });
        table.load();
        tableComponent.table = table;
        tableComponent.onCreate.subscribe(() => {
            createRelatedForm();
        });
    }

    public createRelatedForm(repository: IRepository, table: NgTable, id?: any): EntityNgForm {
        const formRef: ComponentRef<IOFormComponent> = this.dialog.open(IOFormComponent);
        const form: EntityNgForm = this.create(repository);
        form.onSaved.subscribe(() => {
            formRef.destroy();
            table.load();
        });
        form.onDestroyed.subscribe(() => {
            formRef.destroy();
        });
        form.onNotFound.subscribe(() => {
            formRef.destroy();
        });
        if (id) {
            form.load(id);
        }
        formRef.instance.title = repository.endpoint;
        formRef.instance.form = form;
        formRef.instance.onClose.subscribe(() => {
            formRef.destroy();
        });
        return form;
    }

    public create(repository: IRepository): EntityNgForm | null {
        const iNgInputs: INgInput[] = [];
        repository.fields.forEach((field: IRepositoryField) => {
            try {
                const iNgInput: INgInput = this.createInput(field, repository);
                iNgInputs.push(iNgInput);
            } catch (ex) {
                console.error(ex);
            }
        });
        const resource = () => this.api.resource(repository.endpoint);
        return new EntityNgForm(resource, iNgInputs);
    }
}
