import {IRepositoryField} from './repository-field.interface';

export interface IRepository {
    title: string;
    endpoint: string;
    key: string;
    label: string;
    fields: IRepositoryField[];
    columns: string[];
}
