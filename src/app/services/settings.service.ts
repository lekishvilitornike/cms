import {EventEmitter, Injectable} from '@angular/core';
import {LocalStorage} from '../supports/storage';
import {IRepository} from "../ng-repo/repository.interface";

@Injectable()
export class SettingsService {

    private data: any = {};

    public change: EventEmitter<any> = new EventEmitter();

    constructor() {
        this.data.token = LocalStorage.get('token');
        this.data.locale = LocalStorage.get('locale');
    }

    setData(data: any) {
        this.data = Object.assign(this.data, data);
    }

    set(property: string, value: any): void {
        this.data[property] = value;
    }

    get(property: string, $default: any = null) {
        const props = property.split('.');
        let prop: string;
        let data = this.data;
        for (prop of props) {
            if (data.hasOwnProperty(prop)) {
                data = data[prop];
            } else {
                return $default;
            }
        }
        return data;
    }

    set user(user: any) {
        if (user) {
            this.set('token', user.token);
            LocalStorage.set('token', user.token);
        } else {
            this.set('token', null);
            LocalStorage.remove('token');
        }
        this.set('user', user);
        this.change.emit();
    }

    set locale(locale: string) {
        if (this.locales.indexOf(locale) != -1) {
            LocalStorage.set('locale', locale);
            this.set('locale', locale);
        }
        this.change.emit();
    }

    get user(): any {
        return this.get('user');
    }

    get token(): any {
        return this.get('token');
    }

    get navigation(): any {
        return this.get('navigation', []);
    }

    get translations(): any {
        return this.get('translations', []);
    }

    get locales(): any[] {
        return this.get('locales', []);
    }

    get locale(): string {
        return this.get('locale');
    }

    public getRepositoryByName(name: string): IRepository {
        return this.get('repositories.' + name);
    }

    public getFileConfigByName(name: string) {
        return this.get('fileConfigs.' + name);
    }
}
