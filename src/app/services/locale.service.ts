import {Injectable} from "@angular/core";
import {SettingsService} from "./settings.service";
import "rxjs/add/operator/toPromise";

@Injectable()
export class LocaleService {

    constructor(private settings: SettingsService) {
    }

    public getLocales(): any[] {
        return this.settings.locales;
    }

    public getLocale() {
        return this.settings.locale;
    }

    public setLocale(locale: string) {
        this.settings.locale = locale;
    }

    public isLocale(locale: string): boolean {
        return this.settings.locale == locale;
    }
}
