import {Api} from '../api';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {SettingsService} from './settings.service';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

    public errors: any;

    constructor(public api: Api,
                public settings: SettingsService,
                public router: Router) {
    }

    public attempt(form: any) {
        this.api.resource('auth/login').post(form)
            .subscribe(
                (response: any) => {
                    this.settings.user = response;
                    this.router.navigate(['/']);
                    this.errors = null;
                }, (response: any) => {
                    console.log('err', response);
                    this.errors = response.json();
                }
            );
    }

    public logout() {
        this.settings.user = null;
        this.router.navigate(['/login']);
    }
}
