import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {Resource} from './supports/resource';
import {routes} from './routes';
import {Api} from './api';
import {LocaleService} from './services/locale.service';
import {HomePageComponent} from './pages/home/home.component';
import {LoginPageComponent} from './pages/login/login.component';
import {TranslatePipe} from './pipes/translate.pipe';
import {AuthGuard} from './supports/auth-guard';
import {HeaderComponent} from './components/header/header.component';
import {ItemListPageComponent} from './pages/items/list.component';
import {ItemDetailPageComponent} from './pages/items/detail.copmponent';
import {SettingsService} from './services/settings.service';
import {LoadingComponent} from './components/loading/loading.component';
import {AuthService} from './services/auth.service';
import {LeaveDirective} from './directives/leave.directive';
import {RouterModule} from '@angular/router';
import {NgFormModule} from './modules/ng-form/ng-form.module';
import {NgTableModule} from './modules/ng-table/ng-table.module';
import {DialogModule} from './modules/dialog/dialog.module';
import {AlertModule} from './modules/alert/alert.module';
import {PlayerModule} from './modules/player/player.module';
import {DropdownDirective} from './directives/dropdown.directive';
import {NgInputsModule} from './modules/ng-inputs/ng-inputs.module';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {NgRepoModule} from './ng-repo/ng-repo.module';
import {CountdownComponent} from './components/countdown/countdown.component';
import {LeaderBoardComponent} from "./pages/leaderboard/leaderboard.component";

@NgModule({
    declarations: [
        //root component
        AppComponent,

        //components
        LoadingComponent,
        CountdownComponent,

        //partials
        HeaderComponent,

        //pages
        HomePageComponent,
        LoginPageComponent,

        ItemListPageComponent,
        ItemDetailPageComponent,
        LeaderBoardComponent,
        //directives
        LeaveDirective,
        DropdownDirective,

        //pipes
        TranslatePipe,

        SidebarComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        NgTableModule,
        NgFormModule,
        DialogModule,
        AlertModule,
        PlayerModule,
        NgInputsModule,
        NgRepoModule,
        RouterModule.forRoot(routes),
    ],
    providers: [
        Api,
        AuthGuard,
        AuthService,
        LocaleService,
        SettingsService,
        Resource,
        {
            provide: APP_INITIALIZER,
            deps: [Api],
            useFactory: settingsLoaderFactory,
            multi: true
        }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}

export function settingsLoaderFactory(api: Api) {
    return () => api.loadSettings();
}
