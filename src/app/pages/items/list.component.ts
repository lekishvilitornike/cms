import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {NgTable} from '../../modules/ng-table';
import {DatePipe} from '@angular/common';
import {NgTableFactory} from '../../ng-repo/table.factory';
import {IRepository} from '../../ng-repo/repository.interface';
import {SettingsService} from '../../services/settings.service';


@Component({
    templateUrl: 'list.component.html',
})
export class ItemListPageComponent implements OnInit, OnDestroy {

    public repository: IRepository;
    public table: NgTable;

    private subscription: Subscription;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private settings: SettingsService,
                private tableFactory: NgTableFactory) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params: any) => {
            this.open(params['resource']);
        });
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        this.clear();
    }

    clear() {
        this.repository = null;
        this.table = null;
    }

    open(resourceName: string) {
        this.clear();

        this.repository = this.settings.getRepositoryByName(resourceName);

        if (!this.repository) {
            return;
        }

        this.table = this.tableFactory.create(this.repository);
        this.table.columns.unshift({
            type: 'text',
            name: 'id',
            title: 'ID',
            classList: ['col-xs-1', 'text-center'],
            value: (item) => item.id,
        });
        this.table.columns.push({
            type: 'text',
            name: 'created_at',
            title: 'date',
            classList: ['col-xs-1', 'text-center'],
            value: (item) => (new DatePipe('en')).transform(item['created_at'], 'yyyy.MMM.dd HH:mm')
        });
        this.table.columns.push({
            title: 'Edit',
            type: 'button',
            classList: ['col-xs-1'],
            action: (item) => this.router.navigate([this.repository.endpoint, item.id])
        });
        this.table.load();
    }
}
