import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import {EntityNgForm} from '../../modules/ng-form/entity-ng-form';
import {AlertService} from '../../modules/alert/alert.service';
import {IRepository} from '../../ng-repo/repository.interface';
import {SettingsService} from '../../services/settings.service';
import {NgFormFactory} from '../../ng-repo/form.factory';

@Component({
    templateUrl: 'detail.component.html',
})
export class ItemDetailPageComponent implements OnInit, OnDestroy {

    public repository: IRepository;
    public form: EntityNgForm;

    private subscription: Subscription;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private alert: AlertService,
                private formFactory: NgFormFactory,
                private settings: SettingsService) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params: any) => {
            this.open(params['resource'], params['id'] !== 'create' ? params['id'] : null);
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    clear() {
        if (this.form) {
            this.form.dispose();
            this.form = null;
        }
    }

    open(resource: string, id: number | null) {
        this.clear();

        this.repository = this.settings.getRepositoryByName(resource);
        if (!this.repository) {
            return;
        }

        this.form = this.formFactory.create(this.repository);
        if (!this.form) {
            return;
        }

        this.form.onCreated.subscribe(() => {
            this.router.navigate(['/', this.repository.endpoint]).then(() => {
                this.alert.success('created successfully');
            });
        });
        this.form.onUpdated.subscribe(() => {
            this.alert.success('updated successfully');
        });
        this.form.onDestroyed.subscribe(() => {
            this.router.navigate(['/', this.repository.endpoint]).then(() => {
                this.alert.success('deleted successfully');
            });
        });
        this.form.onError.subscribe((message) => {
            this.alert.danger(message);
        });
        this.form.onNotFound.subscribe(() => {
            this.router.navigate(['/', this.repository.endpoint]).then(() => {
                this.alert.warning('not found');
            });
        });
        this.form.load(id);
    }

    save() {
        this.form.save();
    }

    destroy() {
        if (this.form && window.confirm('do you really want to delete')) {
            this.form.destroy();
        }
    }
}
