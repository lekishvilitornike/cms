import {Component, OnInit} from "@angular/core";
import {Api} from "../../api";
import {AuthService} from "../../services/auth.service";

@Component({
    templateUrl: 'login.component.html',
})

export class LoginPageComponent implements OnInit {

    public form: any = {};

    constructor(public api: Api,
                public auth: AuthService) {

    }

    ngOnInit(): any {

    }
}
