import {Component, OnInit} from '@angular/core';
import {Api} from '../../api';
import {Observable} from 'rxjs/Observable';

@Component({
    templateUrl: 'home.component.html',
})
export class HomePageComponent implements OnInit {

    // public resource: (query: any) => Observable<any>;
    // public models: any[];
    // public model: any;

    public content:any;

    constructor(private api: Api) {
    }

    ngOnInit(): any {
        // this.resource = (query) => this.api.resource('tags/suggestions').get(query);
    }
}
