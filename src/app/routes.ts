import {AuthGuard} from './supports/auth-guard';
import {HomePageComponent} from './pages/home/home.component';
import {LoginPageComponent} from './pages/login/login.component';
import {ItemDetailPageComponent} from './pages/items/detail.copmponent';
import {ItemListPageComponent} from './pages/items/list.component';
import {Routes} from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: HomePageComponent,
    }, {
        path: 'login',
        component: LoginPageComponent,
    }, {
        path: ':resource',
        canActivate: [AuthGuard],
        component: ItemListPageComponent,
    }, {
        path: ':resource/:id',
        canActivate: [AuthGuard],
        component: ItemDetailPageComponent,
    }
];
