import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Resource} from './supports/resource';
import {SettingsService} from './services/settings.service';
import {environment} from '../environments/environment';

@Injectable()
export class Api {

    constructor(public http: Http,
                public settings: SettingsService) {
        this.settings.change.subscribe(() => {
            this.loadSettings();
        });
    }

    loadSettings() {
        return this.resource('settings').get()
            .toPromise().then(response => {
                this.settings.setData(response);
            });
    }

    resource(url: string): Resource {
        let resource = new Resource(this.http);
        resource.urlParts = [environment.baseUrl, url];

        let token: string = this.settings.token;
        if (token) {
            resource.headers['Authorization'] = 'bearer ' + token;
        }

        let locale: string = this.settings.locale;
        if (locale) {
            resource.query['locale'] = locale;
            resource.headers['Locale'] = locale;
        }

        return resource;
    }
}
