import {Pipe, PipeTransform} from "@angular/core";
import {SettingsService} from "../services/settings.service";

@Pipe({
    name: 'translate',
    pure: false,
})
export class TranslatePipe implements PipeTransform {

    private key: string;
    private value: string = "";

    constructor(public settings: SettingsService) {
        this.settings.change.subscribe(() => {
            this.value = this.transform(this.key);
        });
    }


    transform(key: any) {
        this.key = key;

        if (this.settings.translations.hasOwnProperty(this.key)) {
            this.value = this.settings.translations[this.key];
        } else {
            this.value = '@' + this.key;
        }

        return this.value;
    }
}