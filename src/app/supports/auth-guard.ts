import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {SettingsService} from '../services/settings.service';


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private settings: SettingsService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (!this.settings.user) {
            this.router.navigate(['/login']);
            return false;
        }

        return true;
    }
}
