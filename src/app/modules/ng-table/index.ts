export {NgTableModule} from './ng-table.module';
export {NgTableComponent} from './components/ng-table/ng-table.component';
export {NgTable, INgColumn} from './ng-table';
