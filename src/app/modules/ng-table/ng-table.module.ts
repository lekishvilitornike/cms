import {NgModule} from '@angular/core';
import {NgTableComponent} from './components/ng-table/ng-table.component';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule
    ],
    declarations: [
        NgTableComponent,
    ],
    exports: [
        NgTableComponent
    ]
})
export class NgTableModule {
}
