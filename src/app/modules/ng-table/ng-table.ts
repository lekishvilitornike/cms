import {Observable} from 'rxjs/Observable';

export interface INgColumn {
    type: string;
    name?: string;
    title?: string;
    value?: (item: any) => string;
    action?: (item: any) => void;
    classList?: string[];
}

export class NgTable {

    public loading: boolean = true;
    public query: any = {};
    public data: any[] = [];
    public currentPage: number = 0;
    public lastPage: number = 0;
    public orderColumn: string;
    public orderDirection: string;
    public error?: string = null;

    constructor(private fetch: (query: any) => Observable<any>,
                public columns: INgColumn[] = []) {
    }

    public load() {
        this.loading = true;
        this.fetch(this.query)
            .subscribe((response: any) => {
                this.loading = false;
                this.setData(response);
            }, (err: any) => {
                this.loading = false;
                try {
                    this.error = err.json().message;
                } catch (e) {
                    this.error = ('(' + err.status + ') ' + err.statusText);
                }
            });
    }

    public setData(response: any) {
        this.data = response.data;
        this.currentPage = response.currentPage;
        this.lastPage = response.lastPage;
        this.orderColumn = response.orderColumn;
        this.orderDirection = response.orderDirection;
    }

    public clear() {
        this.setData([]);
    }

    public setQuery(query: any) {
        this.query = query;
        this.load();
    }

    public setPage(page: number) {
        this.query['page'] = page;
        this.load();
    }

    public setOrder(column: INgColumn) {
        this.query['order'] = column.name;
        this.query['direction'] = (column.name == this.orderColumn) && (this.orderDirection == 'asc') ? 'desc' : 'asc';
        this.load();
    }

    public orderedBy(orderColumn: INgColumn, direction: string) {
        return orderColumn.name == this.orderColumn && direction == this.orderDirection;
    }
}
