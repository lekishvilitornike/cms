import {Component, Input, OnInit} from '@angular/core';
import {NgTable} from '../../ng-table';


@Component({
    selector: 'ng-table',
    templateUrl: 'ng-table.component.html',
    styleUrls: ['ng-table.component.less']
})
export class NgTableComponent implements OnInit {

    @Input()
    public table: NgTable;

    ngOnInit(): any {
    }
}
