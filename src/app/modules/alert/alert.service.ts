import {ComponentFactory, ComponentFactoryResolver, ComponentRef, Injectable} from '@angular/core';
import {AlertOutletComponent} from './components/alert-outlet/alert-outlet.component';
import {AlertComponent} from './components/alert/alert.component';


@Injectable()
export class AlertService {

    private outlet: AlertOutletComponent;
    private componentFactory: ComponentFactory<AlertComponent>;

    constructor(componentFactoryResolver: ComponentFactoryResolver) {
        this.componentFactory = componentFactoryResolver.resolveComponentFactory(AlertComponent);
    }

    public setOutlet(outlet: AlertOutletComponent): void {
        this.outlet = outlet;
    }

    private alert(message: string, type: string, delay: number = 5000): void {
        let alert: ComponentRef<AlertComponent> = this.outlet.container.createComponent(this.componentFactory);
        alert.instance.type = type;
        alert.instance.message = message;
        alert.instance.delay = delay;
        alert.instance.close = () => {
            alert.destroy();
        };
    }

    info(message: string, delay?: number) {
        this.alert(message, 'info', delay);
    }

    success(message: string, delay?: number) {
        this.alert(message, 'success', delay);
    }

    warning(message: string, delay?: number) {
        this.alert(message, 'warning', delay);
    }

    danger(message: string, delay?: number) {
        this.alert(message, 'danger', delay);
    }
}
