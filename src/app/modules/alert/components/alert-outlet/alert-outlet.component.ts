import {AfterViewInit, Component, Inject, ViewChild, ViewContainerRef} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {AlertService} from '../../alert.service';

@Component({
    selector: 'alert-outlet',
    templateUrl: './alert-outlet.component.html',
    styleUrls: ['./alert-outlet.component.less']
})
export class AlertOutletComponent implements AfterViewInit {

    @ViewChild('container', {read: ViewContainerRef})
    public container: ViewContainerRef;

    constructor(private alert: AlertService,
                @Inject(DOCUMENT) private document: Document) {
    }

    ngAfterViewInit(): void {
        this.alert.setOutlet(this);
    }
}
