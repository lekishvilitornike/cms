import {Component, Input} from '@angular/core';

@Component({
    selector: 'alert',
    templateUrl: './alert.component.html',
})
export class AlertComponent {

    @Input()
    public message: string;

    @Input()
    public type: string;

    @Input()
    public delay: number;

    @Input()
    public close: () => void;

    private timer: any;

    ngOnInit() {
        this.startHideTimeout();
    }

    startHideTimeout() {
        this.timer = setTimeout(() => {
            this.close();
        }, this.delay);
    }

    stopHideTimeout() {
        clearTimeout(this.timer);
    }

}
