import {Component, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'io-auto-complete',
    templateUrl: 'auto-complete.component.html',
    styleUrls: ['auto-complete.component.less']
})
export class IOAutoCompleteComponent {

    @Input()
    resource: (query: any) => Observable<any>;

    @Input()
    multiple: boolean = false;

    @Input()
    creating: boolean = true;

    @Input()
    key: string = 'id';

    @Input()
    label: string = 'title';

    @Input()
    model: any;

    @Output()
    modelChange: EventEmitter<any> = new EventEmitter();

    public keyword: string = '';

    public suggestions: any[] = [];

    public index: number = null;

    public loading: boolean = false;

    public isFocused: boolean = false;

    private requestSubscription: Subscription;

    constructor(public element: ElementRef) {
    }

    select(item: any) {
        this.clear();
        if (this.multiple) {
            if (this.model) {
                if (!this.model.find(($item) => $item[this.label] == item[this.label])) {
                    this.model = this.model.concat([item]);
                }
            } else {
                this.model = [item];
            }
        } else {
            this.model = item;
        }
        this.modelChange.emit(this.model);
        this.isFocused = false;
    }

    remove(item: any = null) {
        if (this.multiple) {
            const index = this.model.indexOf(item);
            if (index > -1) {
                this.model.splice(index, 1);
            }
        } else {
            this.model = undefined;
        }
        this.modelChange.emit(this.model);
    }

    clear() {
        this.keyword = '';
        this.suggestions = [];
        this.index = null;
        if (this.requestSubscription) {
            this.requestSubscription.unsubscribe();
        }
        this.loading = false;
    }

    query(): any {
        const query: any = {
            keyword: this.keyword
        };
        if (this.multiple && this.model && this.model.length) {
            query.except = this.model.map((suggestion: any) => suggestion[this.key]);
        }
        return query;
    }

    request() {
        if (this.requestSubscription) {
            this.requestSubscription.unsubscribe();
        }
        const query: any = this.query();
        this.suggestions = [];
        this.index = null;
        this.loading = true;
        this.requestSubscription = this.resource(query)
            .subscribe((response: any) => {
                this.suggestions = response;
                this.index = null;
                this.loading = false;
            });
    }

    @HostListener('document:click', ['$event.target'])
    click(target: HTMLElement) {
        const isFocused = this.element.nativeElement.contains(target);
        if (this.isFocused != isFocused) {
            if (isFocused) {
                this.focus();
            } else {
                this.blur();
            }
        }
    }

    focus() {
        this.isFocused = true;
        // if (this.suggestions.length == 0) {
        // }
        this.request();
        setTimeout(() => {
            const input: HTMLInputElement | undefined = this.element.nativeElement.querySelector('input');
            if (input) {
                input.focus();
            }
        });
    }

    blur() {
        this.isFocused = false;
        this.create();
        this.clear();
    }

    onEnter() {
        if (this.index !== null && this.suggestions[this.index] !== undefined) {
            this.select(this.suggestions[this.index]);
        } else {
            this.create();
        }
    }

    private create() {
        if (this.creating && this.keyword) {
            const item = {};
            item[this.label] = this.keyword;
            this.select(item);
        }
    }

    changeSuggestionIndex(value: number) {
        const length = this.suggestions.length;
        if (length === 0) {
            return;
        }
        this.index = this.index !== null
            ? (this.index + value + length) % length
            : 0;
    }

    onKeyUp(event: KeyboardEvent) {
        switch (event.keyCode) {
            case 38: // arrow up
            {
                event.preventDefault();
                this.changeSuggestionIndex(-1);
                break;
            }
            case 40: // arrow down
            {
                event.preventDefault();
                this.changeSuggestionIndex(1);
                break;
            }
            case 13: // enter
            {
                event.preventDefault();
                this.onEnter();
                break;
            }
            default: {
                this.request();
            }
        }
    }

    onKeyDown($event) {
        if ($event.keyCode == 9) {
            this.blur();
        }
    }
}
