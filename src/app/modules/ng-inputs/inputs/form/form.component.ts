import {Component, EventEmitter, Input, Output} from '@angular/core';
import {EntityNgForm} from '../../../ng-form/entity-ng-form';

@Component({
    selector: 'io-form',
    templateUrl: 'form.component.html',
})
export class IOFormComponent {

    @Input()
    public title: string;

    @Input()
    public form: EntityNgForm;

    @Output()
    public onClose: EventEmitter<void> = new EventEmitter();

    constructor() {
    }

    open(id: any | null) {
        this.form.load(id);
    }

    close() {
        this.onClose.emit();
    }

    save() {
        this.form.save();
    }

    destroy() {
        if (this.form && window.confirm('do you really want to delete')) {
            this.form.destroy();
        }
    }
}
