import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'io-checkbox',
    templateUrl: 'checkbox.component.html',
})
export class IOCheckboxComponent implements OnInit {

    @Input() public model: any;

    @Output() public modelChange = new EventEmitter();

    ngOnInit(): any {
    }

    toggle() {
        this.model = !this.model;

        this.modelChange.emit(this.model);
    }

}