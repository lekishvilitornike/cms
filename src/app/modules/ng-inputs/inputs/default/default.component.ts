import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'io-default',
    templateUrl: 'default.component.html',
})
export class IODefaultComponent implements OnInit {

    @Input() type: string = 'text';

    @Input() model: string;

    @Output() modelChange: EventEmitter<string> = new EventEmitter();

    ngOnInit(): void {
    }

    setModel(model: any) {
        this.model = model;
        this.modelChange.emit(this.model);
    }
}
