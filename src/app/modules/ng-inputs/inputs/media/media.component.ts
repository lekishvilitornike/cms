import {Component, ComponentRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DialogService} from '../../../dialog/dialog.service';
import {UploadDialogComponent} from '../../components/upload-dialog/upload-dialog.component';

export interface IUploadConfig {
    id: number;
    type: string;
    width?: number;
    ratio?: number;
}

@Component({
    selector: 'ng-media',
    templateUrl: 'media.component.html',
    styleUrls: ['media.component.less']
})
export class IOMediaComponent implements OnInit {

    @Input() model: any;

    @Output() modelChange: EventEmitter<any> = new EventEmitter();

    @Input() server: (form: any) => Observable<any>;

    @Input() config: IUploadConfig;

    public form: any = {};

    public isOpen: boolean = false;

    constructor(public dialog: DialogService) {
    }

    ngOnInit() {
        if (!this.config) {
            console.error('config is required');
            return;
        }

        if (!this.config.width) {
            this.config.width = 1920;
        }

        if (!this.config.ratio) {
            this.config.ratio = 16 / 9;
        }
    }

    chooseFile(event: any) {
        this.open(event.target.files[0]);
        event.target.value = '';
    }

    public open(file: any) {
        const componentRef: ComponentRef<UploadDialogComponent> = this.dialog.open(UploadDialogComponent);
        componentRef.instance.file = file;
        componentRef.instance.requestServer = this.server;
        componentRef.instance.config = this.config;
        componentRef.instance.onUploaded.subscribe((response) => {
            this.model = response;
            this.modelChange.emit(this.model);
        });
        componentRef.instance.onClose.subscribe(() => {
            componentRef.destroy();
        });
    }
}
