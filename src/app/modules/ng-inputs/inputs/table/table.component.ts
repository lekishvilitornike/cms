import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgTable} from '../../../ng-table/ng-table';

@Component({
    selector: 'io-table',
    templateUrl: 'table.component.html',
})
export class IOTableComponent {

    @Output()
    public onCreate: EventEmitter<void> = new EventEmitter();

    @Input()
    public table: NgTable;

    create() {
        this.onCreate.emit();
    }
}
