import {Component, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Component({
    selector: 'io-select',
    templateUrl: 'select.component.html',
})
export class IOSelectComponent {

    private _model: string;

    @Input()
    key: string = 'id';

    @Input()
    label: string = 'name';

    @Input()
    options: any[] = [];

    public isOpen: boolean = false;

    @Input()
    set model(value: any) {
        this._model = value;
    }

    get model() {
        return this._model;
    }

    @Output()
    modelChange: EventEmitter<any> = new EventEmitter();

    constructor(private elementRef: ElementRef) {
    }

    select(value: any) {
        this._model = value;
        this.modelChange.emit(this.model);
    }

    @HostListener('document:click', ['$event.target'])
    public click(element: any) {
        this.isOpen = this.elementRef.nativeElement.contains(element) ? !this.isOpen : false;
    }
}
