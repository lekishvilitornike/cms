import {Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2} from '@angular/core';

export interface Day {
    date: Date;
    value: number;
    active: boolean;
    _today: boolean;
    _old: boolean;
    _new: boolean;
}

@Component({
    selector: 'io-date-picker',
    templateUrl: 'date-picker.component.html',
    styleUrls: ['date-picker.component.less'],
})
export class IODatePickerComponent implements OnInit {

    public date: Date;

    public preview: Date;

    public isOpen: boolean = false;

    public weekDayNames: string[] = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

    public weekDays: Day[][] = [];

    private clickOutsideDestroy: () => void;

    @Input()
    set model(date: any) {
        this.date = new Date(date);
        console.log(this.date);
    }

    @Output() public modelChange: EventEmitter<Date> = new EventEmitter();

    constructor(private renderer: Renderer2, private ref: ElementRef) {
    }

    ngOnInit(): void {
    }

    render(date: Date) {
        this.preview = date;
        const now: Date = new Date;
        const firstDate: Date = new Date(this.preview.getFullYear(), this.preview.getMonth());
        firstDate.setDate(-((firstDate.getDay() + 6) % 7) + 1);
        let i: number;
        let j: number;
        let k: number;
        const itDate: Date = firstDate;
        for (i = 0; i < 42; i++) {
            j = Math.floor(i / 7);
            k = Math.floor(i % 7);
            if (!this.weekDays[j]) {
                this.weekDays[j] = [null, null, null, null, null, null, null];
            }
            this.weekDays[j][k] = {
                date: new Date(itDate),
                value: itDate.getDate(),
                active: this.sameDay(this.date, itDate),
                _today: this.sameDay(now, itDate),
                _old: itDate.getMonth() > this.preview.getMonth(),
                _new: itDate.getMonth() < this.preview.getMonth(),
            };
            itDate.setDate(itDate.getDate() + 1);
        }
    }

    open() {
        if (this.date.toString() === 'Invalid Date') {
            this.date = new Date();
        }
        this.render(this.date);
        this.isOpen = true;
        this.clickOutsideDestroy = this.renderer.listen(document, 'click', (event) => {
            if (!this.ref.nativeElement.contains(event.target)) {
                this.close();
            }
        });
    }

    close() {
        this.isOpen = false;
        this.preview = null;
        this.weekDays = [];
        this.clickOutsideDestroy();
    }

    prevMonth() {
        this.render(new Date(this.preview.getFullYear(), this.preview.getMonth() - 1));
    }

    nextMonth() {
        this.render(new Date(this.preview.getFullYear(), this.preview.getMonth() + 1));
    }

    activate(day: Day) {
        this.date = day.date;
        this.modelChange.emit(this.date);
        this.close();
    }

    sameDay(d1: Date, d2: Date) {
        return d1.toDateString() === d2.toDateString();
    }
}
