import Parchment from 'parchment';
import * as  Quill from 'quill';
let Embed: any = Quill.import('blots/block/embed');

class AbstractEmbedBlot extends Parchment.Embed {
}
AbstractEmbedBlot.prototype = Embed.prototype;


class EmbedBlot extends AbstractEmbedBlot {

    static blotName = 'embed';
    static tagName = 'IFRAME';

    static create(value) {

        let node: any = super.create(value);
        node.value = value;

        function onLoad() {
            if (node.contentWindow) {
                node.style.height = (node.contentWindow.document.body.firstChild.offsetHeight) + 'px';
                setTimeout(onLoad, 1000);
            }
        }

        node.addEventListener('load', () => {
            node.style.display = 'block';
            node.style.width = '100%';
            node.style.maxWidth = '800px';
            node.style.margin = '0 auto 5px';
            node.style.padding = '0';
            node.style.border = 'solid 5 px #F00';
            node.frameBorder = '0';
            node.contentWindow.document.open();
            node.contentWindow.document.write('<div style="display: flex; justify-content: center;">' + value + '</div>');
            node.contentWindow.document.body.style.width = '100%';
            node.contentWindow.document.body.style.overflow = 'hidden';
            node.contentWindow.document.body.style.boxSizing = 'border-box';
            node.contentWindow.document.body.style.margin = '0';
            node.contentWindow.document.body.style.padding = '0';
            node.style.height = (node.contentWindow.document.body.firstChild.offsetHeight) + 'px';
            node.contentWindow.document.close();
            onLoad();
        });

        return node;
    }

    static value(domNode) {
        return domNode.value;
    }
}

export default EmbedBlot;