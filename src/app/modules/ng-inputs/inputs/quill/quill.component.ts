import {AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import EmbedBlot from './blots/embed.blot';
import * as Quill from 'quill';

Quill.register(EmbedBlot);

@Component({
    selector: 'io-quill',
    templateUrl: 'quill.component.html',
    styleUrls: ['quill.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class IOQuillComponent implements AfterViewInit {

    @ViewChild('container') container: ElementRef;
    @ViewChild('embedContainer') embedContainer: ElementRef;

    private _model: any;

    public editor: Quill;

    @Input()
    set model(value) {
        if (this._model !== value) {
            this._model = value;
            this.updateContent();
        }
    };

    get model() {
        return this._model;
    }

    @Output()
    public modelChange: EventEmitter<string> = new EventEmitter;

    public embed: string = '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/396615804&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>';

    constructor() {
    }

    public ngAfterViewInit() {
        this.editor = new Quill(this.container.nativeElement, {
            theme: 'snow',
            modules: {
                toolbar: [
                    ['bold', 'italic', 'underline', 'strike'],     // toggled buttons
                    [{'list': 'ordered'}, {'list': 'bullet'}],
                    [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
                    [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
                    [{'size': ['small', false, 'large', 'huge']}], // custom dropdown
                    [{'header': [1, 2, 3, 4, 5, 6, false]}],
                    [{'color': []}, {'background': []}],           // dropdown with defaults from theme
                    [{'font': []}],
                    [{'align': []}],
                    ['clean'],                                     // remove formatting button
                    ['link', 'image', 'video']                     // link and image, video
                ]
            },
        });

        this.editor.on('text-change', () => {
            this._model = JSON.stringify(this.editor.getContents());
            this.modelChange.emit(this._model);
        });

        this.updateContent();
    }

    updateContent() {
        try {
            let content: any = JSON.parse(this._model);
            if (this.editor) {
                content = Object.assign(this.editor.getContents(), content);
                this.editor.setContents(content);
            }
        } catch (e) {
        }
    }

    addEmbed() {
        let length = this.editor.getLength() -1;
        this.editor.insertEmbed(length, 'embed', this.embed);
    }

    onEmbedChange(content: string) {
        this.embed = content;
    }
}
