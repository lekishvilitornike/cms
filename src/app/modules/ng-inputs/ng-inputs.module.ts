import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IOAutoCompleteComponent} from './inputs/auto-complete/auto-complete.component';
import {IOCheckboxComponent} from './inputs/checkbox/checkbox.component';
import {IOSelectComponent} from './inputs/select/select.component';
import {IODefaultComponent} from './inputs/default/default.component';
import {IODatePickerComponent} from './inputs/datepicker/date-picker.component';
import {CropComponent} from './components/crop/crop.component';
import {IOMediaComponent} from './inputs/media/media.component';
import {DialogModule} from '../dialog/dialog.module';
import {UploadService} from './services/upload.service';
import {HttpClientModule} from '@angular/common/http';
import {IOQuillComponent} from './inputs/quill/quill.component';
import {IOTableComponent} from './inputs/table/table.component';
import {NgTableModule} from '../ng-table/ng-table.module';
import {PaginationComponent} from './components/pagination/pagination.component';
import {NgFormModule} from '../ng-form/ng-form.module';
import {IOFormComponent} from './inputs/form/form.component';
import {UploadDialogComponent} from './components/upload-dialog/upload-dialog.component';
import {NgRepoModule} from '../../ng-repo/ng-repo.module';

const inputs: any[] = [
    CropComponent,
    PaginationComponent,
    UploadDialogComponent,

    IOAutoCompleteComponent,
    IOCheckboxComponent,
    IODatePickerComponent,
    IODefaultComponent,
    IOSelectComponent,
    IOQuillComponent,
    IOMediaComponent,
    IOTableComponent,
    IOFormComponent,
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DialogModule,
        HttpClientModule,
        NgTableModule,
        NgFormModule,
        NgRepoModule,
    ],
    providers: [
        UploadService
    ],
    declarations: inputs,
    exports: inputs,
    entryComponents: inputs,
})
export class NgInputsModule {
}
