import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {UploadService} from '../../services/upload.service';
import {Observable} from 'rxjs/Observable';
import {UploadConfigInterface} from '../../interfaces/upload-config.interface';
import {Subscription} from 'rxjs/Subscription';


@Component({
    selector: 'ng-upload',
    templateUrl: 'upload-dialog.component.html'
})
export class UploadDialogComponent implements OnInit, OnDestroy {

    @Input() file: any;
    @Input() requestServer: (form: any) => Observable<any>;
    @Input() config: UploadConfigInterface;
    @Output() onUploaded: EventEmitter<any> = new EventEmitter();
    @Output() onClose: EventEmitter<any> = new EventEmitter();
    public form: any = {};

    private requestServerSubscription: Subscription;
    private uploadSubscription: Subscription;
    private inProgress: boolean = false;

    constructor(public uploader: UploadService) {
    }

    ngOnInit() {
        if (!this.config) {
            console.error('config is required');
            return;
        }
        console.log(this.config);
        this.form = {
            type_id: this.config.id,
            watermarkPosition: null,
            crop: null
        };
        if (this.config.type === 'image' || this.config.type === 'audio') {
          this.uploader.setFile(this.file);
        } else {
          this.uploader.setFile(this.file, false);
        }
    }

    ngOnDestroy() {
        if (this.requestServerSubscription) {
            this.requestServerSubscription.unsubscribe();
        }
        if (this.uploadSubscription) {
            this.uploadSubscription.unsubscribe();
        }
    }

    public upload() {
        if (this.inProgress) return;
        this.inProgress = true;
        this.requestServerSubscription = this.requestServer(this.form).subscribe((response: any) => {
            this.uploadSubscription = this.uploader.upload(response.server).subscribe((response) => {
                this.onUploaded.emit(response);
                this.close();
            });
        });
    }

    public close() {
        this.onClose.emit();
    }

    setCrop(crop: any) {
        this.form.crop = crop;
    }
}
