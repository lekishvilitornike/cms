import {Component, Input, Output, OnInit, EventEmitter, ElementRef, Renderer2} from "@angular/core";
import {isUndefined} from "util";


@Component({
    selector: 'crop',
    templateUrl: 'crop.component.html',
    styleUrls: ["crop.component.less"]
})
export class CropComponent implements OnInit {

    @Input() src: string;
    @Input() ratio: number;
    @Output() output: EventEmitter<any> = new EventEmitter();

    public image: any;
    public element: any;
    public containerWidth: number;
    public containerHeight: number;

    public left: number = 0;
    public top: number = 0;
    public width: number;
    public height: number;

    public zoomMax: number;
    public zoomMin: number;
    public zoomStep: number;
    public zoom: number;

    private destroyMousemove: any;
    private destroyMouseup: any;

    constructor(public renderer: Renderer2,
                element: ElementRef) {
        this.element = element.nativeElement;
    }

    ngOnInit() {
        this.image = new Image();
        this.image.src = this.src;
        this.renderer.listen(this.image, "load", () => {
            this.loaded();
        });
    }

    loaded() {
        let maxWidth: number = this.element.offsetWidth;
        let maxHeight: number = this.element.offsetHeight;
        let containerRatio: number = maxWidth / maxHeight;
        console.log(maxWidth,maxHeight,this.ratio);
        if (containerRatio > this.ratio) {
            this.containerWidth = Math.floor(maxHeight * this.ratio);
            this.containerHeight = maxHeight;
        } else {
            this.containerWidth = maxWidth;
            this.containerHeight = Math.floor(maxWidth / this.ratio);
        }

        let z1 = this.containerWidth / this.image.width;
        let z2 = this.containerHeight / this.image.height;
        this.zoomMin = Math.max(z1, z2);
        this.zoomMax = Math.max(2, Math.max(z1, z2));
        this.zoomStep = (this.zoomMax - this.zoomMin) / 100;
        this.setZoom(Math.max(z1, z2));
    }

    calc() {
        let obj: any = {
            x: Math.floor(this.left / this.zoom),
            y: Math.floor(this.top / this.zoom),
            w: Math.floor(this.containerWidth / this.zoom),
            h: Math.floor(this.containerHeight / this.zoom)
        };
        this.output.emit(obj);
    }

    setZoom(zoom: any) {
        this.zoom = parseFloat(zoom);
        let width = this.image.width * this.zoom;
        let height = this.image.height * this.zoom;
        let left: number = !isUndefined(this.width) ? ((width - this.width) / 2) : 0;
        let top: number = !isUndefined(this.width) ? ((height - this.height) / 2) : 0;

        this.setX(this.left + left);
        this.setY(this.top + top);
        this.width = width;
        this.height = height;
        this.calc();
    }

    setX(value: any) {
        let max = this.image.width * this.zoom - this.containerWidth;
        this.left = this.setBetween(value, 0, max);
    }

    setY(value: any) {
        let max = this.image.height * this.zoom - this.containerHeight;
        this.top = this.setBetween(value, 0, max);
    }

    setBetween(value: number, from: number, to: number) {
        return Math.max(Math.min(value, to), from);
    }

    dragBegin(beginEvent: any) {
        let position = {
            x: this.left,
            y: this.top,
        };
        this.destroyMousemove = this.renderer.listen('document', "mousemove", (currentEvent: any) => {
            this.drag(beginEvent, currentEvent, position);
        });
        this.destroyMouseup = this.renderer.listen('document', "mouseup", () => {
            this.dragEnd();
        });
    }

    dragEnd() {
        if (this.destroyMousemove) {
            this.destroyMousemove();
            this.destroyMousemove = null;
        }
        if (this.destroyMouseup) {
            this.destroyMouseup();
            this.destroyMouseup = null;
        }
    }

    drag(beginEvent: any, currentEvent: any, position: any) {
        this.setX(position.x + beginEvent.screenX - currentEvent.screenX);
        this.setY(position.y + beginEvent.screenY - currentEvent.screenY);
        this.calc();
    }
}