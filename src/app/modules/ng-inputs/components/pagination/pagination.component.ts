import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

interface Page {
    title: string;
    value?: number;
    active?: boolean;
}

@Component({
    selector: 'pagination',
    templateUrl: 'pagination.component.html',
})
export class PaginationComponent implements OnInit {

    private _currentPage: number = 0;

    private _lastPage: number = 0;

    private _pages: Page[] = [];

    @Output()
    public change: EventEmitter<number> = new EventEmitter();

    @Input()
    set currentPage(value: number) {
        this._currentPage = value;
        this.updatePages();
    }

    get currentPage(): number {
        return this._currentPage;
    }

    @Input()
    set lastPage(value: number) {
        this._lastPage = value;
        this.updatePages();
    }

    get lastPage(): number {
        return this._lastPage;
    }

    get pages(): Page[] {
        return this._pages;
    }

    ngOnInit(): void {
        this.updatePages();
    }

    updatePages() {
        this._pages = this.compile(this.currentPage, this.lastPage);
    }

    compile(current: number, last: number): Page[] {

        const pages: Page[] = [],
            jump: Page = {title: '...'},
            show: number = 3,
            left: number = Math.max(1, current - show),
            right: number = Math.min(last, current + show);

        let i: number = 1;

        while (i <= last) {
            pages.push({
                title: '' + i,
                value: i,
                active: i === current
            });

            if (i < left) {
                pages.push(jump);
                i = left;
            } else if (right <= i && i !== last) {
                pages.push(jump);
                i = last;
            } else {
                i++;
            }
        }

        return pages;
    }

    setPage(value: any) {
        if (value > 0 && value <= this.lastPage) {
            this.change.emit(value);
        }
    }
}
