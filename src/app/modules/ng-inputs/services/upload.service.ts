import {Observable} from 'rxjs/Observable';
import {Subscriber} from 'rxjs/Subscriber';
import {Subject} from 'rxjs/Subject';
import {Injectable} from '@angular/core';
import {HttpClient, HttpEventType, HttpRequest, HttpResponse} from '@angular/common/http';

@Injectable()
export class UploadService {

    public preview: any;

    public progress: Subject<number> = new Subject();

    public inProgress: boolean = false;

    public errorMessage: string;

    private file: File;

    constructor(private http: HttpClient) {

    }

    setFile(file: File, withPreview: boolean = true) {
        this.file = file;
        this.errorMessage = null;
        this.preview = null;

        if (withPreview) {
            let reader = new FileReader();
            reader.addEventListener('load', (event: any) => {
                this.preview = event.target.result;
            });
            reader.readAsDataURL(this.file);
        }
    }

    public upload(serverUrl: string): Observable<any> {
        this.errorMessage = null;
        if (this.inProgress) {
            return;
        }
        this.inProgress = true;
        return Observable.create((subscriber: Subscriber<any>) => {
            const formData: FormData = new FormData;
            formData.set('file', this.file);
            const req = new HttpRequest('POST', serverUrl, formData, {
                reportProgress: true,
            });
            this.http.request(req).subscribe((res: any) => {
                if (res.type === HttpEventType.UploadProgress) {
                    this.progress.next(Math.round(100 * res.loaded / res.total));
                } else if (res instanceof HttpResponse) {
                    const data: any = res.body;
                    this.preview = data.url;
                    this.progress.next(100);
                    this.inProgress = false;
                    subscriber.next(data);
                    subscriber.complete();
                }
            }, (error) => {
                this.inProgress = false;
                this.progress.next(0);
                this.errorMessage = error.error.message || error.message;
                subscriber.complete();
            });
        });
    }
}
