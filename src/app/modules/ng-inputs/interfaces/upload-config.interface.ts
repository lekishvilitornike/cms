export interface UploadConfigInterface {
    id: number;
    type: string;
    width?: number;
    ratio?: number;
}
