import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AudioPlayerComponent} from './components/audio-player/audio-player.component';
import {PlayerService} from './player.service';
import {SeekerComponent} from './components/seeker/seeker.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule
    ],
    exports: [
        AudioPlayerComponent,
    ],
    declarations: [
        AudioPlayerComponent,
        SeekerComponent,
    ],
    providers: [
        PlayerService
    ]
})
export class PlayerModule {
}
