import {Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, Renderer} from '@angular/core';

@Component({
    selector: 'seeker',
    templateUrl: 'seeker.component.html',
    styleUrls: ['seeker.component.less']
})
export class SeekerComponent implements OnInit {

    @Input() max: number = 100;

    @Input() model: number;

    @Output() modelChange: EventEmitter<number> = new EventEmitter();

    private rect: any;

    private destroyMouseMove: any;

    private destroyMouseUp: any;

    constructor(@Inject(ElementRef) public element: ElementRef,
                @Inject(Renderer) public renderer: Renderer) {

    }

    ngOnInit(): any {
        this.renderer.listen(this.element.nativeElement, 'mousedown', (event: any) => this.begin(event));
    }

    begin(event: any) {
        let process = 'mousemove', end = 'mouseup';
        this.rect = this.element.nativeElement.getBoundingClientRect();
        this.end();
        this.destroyMouseMove = this.renderer.listenGlobal('document', process, (event: any) => this.process(event));
        this.destroyMouseUp = this.renderer.listenGlobal('document', end, () => this.end());
        this.process(event);
    };

    end() {
        if (this.destroyMouseMove) {
            this.destroyMouseMove();
            this.destroyMouseMove = null;
        }
        if (this.destroyMouseUp) {
            this.destroyMouseUp();
            this.destroyMouseUp = null;
        }
    };

    process(e: any) {
        let value = e.clientX - this.rect.left;
        value = Math.max(value, 0);
        value = Math.min(value, this.rect.width);
        value = value / this.rect.width;
        this.model = this.max * value;
        this.modelChange.emit(this.model);
    };

}