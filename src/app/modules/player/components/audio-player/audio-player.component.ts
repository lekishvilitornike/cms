import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {PlayerService} from '../../player.service';

@Component({
    selector: 'audio-player',
    templateUrl: 'audio-player.component.html',
})
export class AudioPlayerComponent implements OnInit {

    @ViewChild('audio') audioRef: ElementRef;

    constructor(public player: PlayerService,
                private renderer: Renderer2) {
    }

    ngOnInit() {
        this.player.setAudioElement(this.audioRef.nativeElement);
        this.renderer.listen(this.player.audio, 'timeupdate', () => {
        });
    }
}