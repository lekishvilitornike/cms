import {Injectable} from '@angular/core';


@Injectable()
export class PlayerService {

    public audio: HTMLAudioElement;

    public src: string;

    private _isActive: boolean = false;
    private _isInited: boolean = false;

    get isActive() {
        return this._isActive;
    }

    get isInited() {
        return this._isInited;
    }

    constructor() {
        this.src = './assets/audio2.mp3';
    }

    setAudioElement(audio: HTMLAudioElement) {
        this.audio = audio;
        this.audio.src = this.src;
        this._isInited = true;
    }

    get isPlaying() {
        return !this.audio.paused
            && !this.audio.ended
            && this.audio.readyState > 2;
    }

    toggle() {
        this._isActive = true;
        if (this.isPlaying) {
            this.audio.pause();
        } else {
            this.audio.play();
        }
    }
}
