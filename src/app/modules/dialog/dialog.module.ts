import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {DialogService} from './dialog.service';
import {BrowserModule} from '@angular/platform-browser';
import {DialogOutletComponent} from './components/dialog-outlet/dialog-outlet.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        BrowserModule
    ],
    exports: [
        DialogOutletComponent
    ],
    declarations: [
        DialogOutletComponent
    ],
    providers: [
        DialogService
    ]
})
export class DialogModule {
}
