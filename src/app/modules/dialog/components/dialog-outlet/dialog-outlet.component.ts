import {AfterViewInit, Component, Inject, ViewChild, ViewContainerRef} from '@angular/core';
import {DialogService} from '../../dialog.service';
import {DOCUMENT} from '@angular/common';

@Component({
    selector: 'dialog-outlet',
    templateUrl: './dialog-outlet.component.html',
    styleUrls: ['./dialog-outlet.component.less']
})
export class DialogOutletComponent implements AfterViewInit {


    @ViewChild('container', {read: ViewContainerRef})
    public container: ViewContainerRef;

    public isOpen: boolean = false;

    constructor(private dialogService: DialogService,
                @Inject(DOCUMENT) private document: Document) {

    }

    ngAfterViewInit(): void {
        this.dialogService.setOutlet(this);
    }

    open() {
        if (!this.isOpen) {
            this.isOpen = true;
            this.document.body.classList.add('modal-open');
        }
    }

    close() {
        if (this.isOpen) {
            this.isOpen = false;
            this.document.body.classList.remove('modal-open');
        }
    }
}
