import {ComponentFactory, ComponentFactoryResolver, ComponentRef, Injectable, Type} from '@angular/core';
import {DialogOutletComponent} from './components/dialog-outlet/dialog-outlet.component';


@Injectable()
export class DialogService {

    private outlet: DialogOutletComponent;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    }

    public setOutlet(outlet: DialogOutletComponent): void {
        this.outlet = outlet;
    }

    open<T>(component: Type<T>): ComponentRef<T> {

        const componentFactory: ComponentFactory<T> = this.componentFactoryResolver.resolveComponentFactory(component);


        const componentRef: ComponentRef<T> = this.outlet.container.createComponent(componentFactory);

        this.outlet.open();

        componentRef.onDestroy(() => {
            if (this.outlet.container.length == 0) {
                this.outlet.close();
            }
        });

        return componentRef;
    }
}
