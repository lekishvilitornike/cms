import {
    AfterViewInit,
    Component,
    ComponentFactory,
    ComponentFactoryResolver,
    ComponentRef,
    EventEmitter,
    Input,
    OnInit,
    Type,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {NgInput} from '../../ng-input';
import {NgFormComponent} from '../ng-form/ng-form.component';
import {NgForm} from '../../ng-form';

export interface InputType {
    model: any;
    modelChange: EventEmitter<any>;
    ngForm: any;
}

@Component({
    selector: 'ng-input',
    templateUrl: './ng-input.component.html'
})
export class NgInputComponent implements OnInit {

    @Input()
    public name: string;

    @Input()
    public input: NgInput;

    @ViewChild('container', {read: ViewContainerRef})
    public container: ViewContainerRef;

    public ngForm: NgForm;

    constructor(public resolver: ComponentFactoryResolver,
                formComponent: NgFormComponent) {
        this.ngForm = formComponent.form;
    }

    ngOnInit() {
        if (this.name) {
            this.input = this.ngForm.getInput(this.name);
        }

        if (!this.input) {
            console.error('input not found');
            return;
        }

        if (!this.input.hidden) {
            this.create(this.input.component);
        }
    }

    create(component: Type<InputType>) {
        const componentFactory: ComponentFactory<InputType> = this.resolver.resolveComponentFactory(component);
        const ref: ComponentRef<InputType> = this.container.createComponent(componentFactory);

        ref.instance.ngForm = this.ngForm;
        ref.instance.model = this.input.value;
        this.input.change.subscribe((value: any) => {
            ref.instance.model = value;
        });

        if (ref.instance.hasOwnProperty('modelChange')) {
            ref.instance.modelChange.subscribe((value: any) => {
                this.input.value = value;
            });
        }

        this.input.instance = ref.instance;
        this.input.created();
    }
}
