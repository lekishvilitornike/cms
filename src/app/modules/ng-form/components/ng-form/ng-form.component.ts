import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '../../ng-form';

@Component({
    selector: 'ng-form',
    templateUrl: './ng-form.component.html'
})
export class NgFormComponent implements OnInit {

    @Input()
    form: NgForm;

    @Input()
    auto: boolean = false;

    @Input()
    size: string = 'col-xs-12';

    ngOnInit() {
    }
}
