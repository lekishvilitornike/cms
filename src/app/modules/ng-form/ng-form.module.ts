import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgFormComponent} from './components/ng-form/ng-form.component';
import {NgInputComponent} from './components/ng-input/ng-input.component';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        NgFormComponent,
        NgInputComponent
    ],
    exports: [
        NgFormComponent,
        NgInputComponent
    ]
})
export class NgFormModule {
}
