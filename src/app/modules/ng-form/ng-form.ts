import {INgInput, NgInput} from './ng-input';
import {EventEmitter} from '@angular/core';

export class NgForm {

    private data: Map<string, NgInput> = new Map();

    public inputs: NgInput[] = [];

    public onSubmit: EventEmitter<{ [property: string]: any }> = new EventEmitter();

    public change: EventEmitter<any> = new EventEmitter();

    public isLocked: boolean = false;

    constructor(inputs: INgInput[] = []) {
        inputs.forEach((inputMeta: INgInput) => {
            const input: NgInput = new NgInput(inputMeta, this);
            this.data.set(inputMeta.name, input);
            this.inputs.push(input);
        });
    }

    public dispose() {
        this.onSubmit.unsubscribe();
        this.change.unsubscribe();
    }

    public setData(data: { [property: string]: any }) {
        this.data.forEach((input: NgInput, key: string) => {
            input.value = data.hasOwnProperty(key)
                ? data[key]
                : null;
        });
    }

    public getData(): { [property: string]: any } {
        const data: { [property: string]: any } = {};
        this.data.forEach((input: NgInput, key: string) => {
            data[key] = input.value;
        });
        return data;
    }

    public getInput(name: string) {
        return this.data.get(name);
    }

    public setValue(name: string, value: any, force: boolean = false): void {
        if (force || this.data.has(name)) {
            this.data.get(name).value = value;
        }
    }

    public getValue(name: string) {
        return this.data.has(name)
            ? this.data.get(name).value
            : null;
    }

    public setErrors(errors: { [property: string]: string[] }) {
        this.data.forEach((input: NgInput, key: string) => {
            if (errors.hasOwnProperty(key)) {
                input.errors = errors[key];
            }
        });
    }

    public reset() {
        this.data.forEach((input: NgInput) => {
            input.value = null;
        });
    }

    public submit() {
        this.onSubmit.emit(this.getData());
    }

    lock() {
        this.isLocked = true;
    }

    unlock() {
        this.isLocked = false;
    }
}
