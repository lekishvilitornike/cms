import {EventEmitter, Type} from '@angular/core';
import {NgForm} from './ng-form';

export interface INgInput {
    name: string;
    component: Type<any>;
    value?: any;
    params: { [key: string]: any };
    created?: (NgInput) => void;
    hidden?: boolean;
}

export class NgInput {

    public change: EventEmitter<any> = new EventEmitter();

    public instance: any;

    private _value: any;
    private _params: { [key: string]: any };

    private _errors: string[] = [];
    private _hasErrors: boolean = false;

    constructor(private meta: INgInput, public form: NgForm) {
        this._value = this.meta.value;
        this._params = this.meta.params;
    }

    public doSetParams() {
        if (this.instance && this._params) {
            Object.keys(this._params).forEach((property: string) => {
                this.instance[property] = this._params[property];
            });
        }
    }

    public created() {
        this.doSetParams();
        if (this.meta.created) {
            this.meta.created(this);
        }
    }

    public get component() {
        return this.meta.component;
    }

    public get hidden() {
        return this.meta.hidden;
    }

    public get name() {
        return this.meta.name;
    }

    public set value(value: any) {
        this._value = value;
        this.change.emit(this._value);
        this.form.change.emit();
        this.errors = [];
    }

    public get value() {
        return this._value;
    }

    public set errors(errors: string[]) {
        this._errors = errors;
        this._hasErrors = errors.length > 0;
    }

    public get errors(): string[] {
        return this._errors;
    }

    public get hasErrors() {
        return this._hasErrors;
    }
}
