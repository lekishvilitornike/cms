import {Resource} from '../../supports/resource';
import {INgInput} from './ng-input';
import {NgForm} from './ng-form';
import {EventEmitter} from '@angular/core';

export class EntityNgForm extends NgForm {

    private _id: number | null;

    public onLoad: EventEmitter<EntityNgForm> = new EventEmitter();
    public onCreated: EventEmitter<{ [property: string]: any }> = new EventEmitter();
    public onUpdated: EventEmitter<{ [property: string]: any }> = new EventEmitter();
    public onSaved: EventEmitter<{ [property: string]: any }> = new EventEmitter();
    public onDestroyed: EventEmitter<boolean> = new EventEmitter();
    public onError: EventEmitter<string> = new EventEmitter();
    public onNotFound: EventEmitter<boolean> = new EventEmitter();

    public get id(): number | null {
        return this._id;
    }

    constructor(private resourceFactory: () => Resource,
                inputs: INgInput[] = []) {
        super(inputs);
    }

    public dispose() {
        super.dispose();
        this.onLoad.unsubscribe();
        this.onCreated.unsubscribe();
        this.onUpdated.unsubscribe();
        this.onSaved.unsubscribe();
        this.onDestroyed.unsubscribe();
        this.onError.unsubscribe();
        this.onNotFound.unsubscribe();
    }

    private resource(): Resource {
        return this.resourceFactory();
    }

    public load(id: number | null) {
        if (!id) return;
        this.lock();
        this.resource().find(id)
            .subscribe((resource: any) => {
                this._id = id;
                this.setData(resource);
                this.unlock();
                this.onLoad.emit(this);
            }, this.errorHandler());
    }

    public save() {
        if (this.isLocked) return;
        this.lock();
        const data = this.getData();
        if (!this.id) {
            this.resource().store(data)
                .subscribe(this.created(), this.errorHandler());
        } else {
            this.resource().update(this.id, data)
                .subscribe(this.updated(), this.errorHandler());
        }
    }

    public destroy() {
        if (this.isLocked) return;
        if (this.id) {
            this.resource().destroy(this.id)
                .subscribe(this.destroyed(), this.errorHandler());
        }
    }

    private created() {
        return (data: any) => {
            this.setData(data);
            this.onCreated.emit(data);
            this.onSaved.emit(data);
            this.unlock();
        };
    }

    private updated() {
        return (data: any) => {
            this.setData(data);
            this.onUpdated.emit(data);
            this.onSaved.emit(data);
            this.unlock();
        };
    }

    private destroyed() {
        return () => {
            this.onDestroyed.emit(true);
            this.unlock();
        };
    }

    private errorHandler() {
        return (error: Response) => {
            let message: string;
            switch (error.status) {
                case 422: {
                    const data: any = error.json();
                    this.setErrors(data);
                    message = Object.keys(data).map((key) => {
                        return data[key].join('<br>');
                    }).join('<br>');
                    break;
                }
                case 404: {
                    this.onNotFound.emit(true);
                    break;
                }
                default: {
                    message = this.getMessageFromErrorResponse(error);
                }
            }
            this.onError.emit(message);
            this.unlock();
        };
    }

    private getMessageFromErrorResponse(error: Response) {
        try {
            const data: any = error.json();
            return data.message;
        } catch (e) {
            console.error(error);
            return error.status + ' (' + error.statusText + ')';
        }
    }
}
